package task1;

public class PingPong{
    private boolean flag;
    PingPong()
    {
        this.flag = true;
    }
    
    Logger logger = Logger.getLogger(PingPong.class.getName());

    public void ping() throws InterruptedException
    {
        synchronized (this)
        {
            while (!flag)
            {
                wait();
            }
            logger.info("ping");
            flag = false;
            Thread.sleep(1000);
            notify();
        }
    }

    public void pong() throws InterruptedException
    {
        synchronized (this)
        {
            while (flag)
            {
                wait();
            }

            logger.info("pong");
            flag = true;
            Thread.sleep(1000);
            notify();
        }
    }
}
