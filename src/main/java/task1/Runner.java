package task1;

public class Runner extends Thread{
    @Override
    public void run(){
        PingPong pingPong = new PingPong();
        while (true)
        {
            try {
                pingPong.ping();
                pingPong.pong();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new Runner().start();
    }
}
