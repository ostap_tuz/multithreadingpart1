package task2;

import java.util.logging.Logger;

public class FibonacciThread {

    Logger logger = Logger.getLogger(FibonacciThread.class.getName());

    private int n;

    FibonacciThread(int n)
    {
        this.n = n;
    }

    public void calculateFibonacci() {
        synchronized (this)
        {
            int firstNumber = 1;
            int secondNumber = 1;
            int result = 0;
            logger.info(String.valueOf(firstNumber));
            logger.info(String.valueOf(secondNumber));
            for (int i = 0; i < n - 2; i++) {
                result = firstNumber+secondNumber;
                logger.info(String.valueOf(result));
                firstNumber = secondNumber;
                secondNumber = result;
            }
        }
    }


}