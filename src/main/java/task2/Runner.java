package task2;

public class Runner extends Thread{

    @Override
    public void run() {
        FibonacciThread fibonacciThread = new FibonacciThread(7);
        fibonacciThread.calculateFibonacci();
    }

    public static void main(String[] args) {
        Runner firstThread = new Runner();
        Runner secondThread = new Runner();
        Runner thirdThread = new Runner();

        try {
            firstThread.start();
            sleep(1000);
            secondThread.start();
            sleep(1000);
            thirdThread.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }



        try {
            firstThread.join();
            secondThread.join();
            thirdThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
